Bienvenue sur E-Kitty, le site de vente de chats pixelisés.

## use

version prod : https://e-kitty.herokuapp.com/
on verra ca plus tard

## branches git
Commentaire des commit en anglais et commençant par : <br>

| Préfixe du commentaire | Dans quel cas l'utiliser                        |
| :--------------------: | :----------------------                         |
|[Add]                   | Ajout d'une nouvelle fonctionnalité             |
|[Update]                | Modification d'une fonctionnalité qui fonctionne|
|[Fix]                   | Modification d'une fonctionnalité qui bug       |
|[Delete]                | Suppression d'une fonctionnalité                |


```mermaid
graph LR;
    A(git clone)-->B(git Branch XXX);
    B-->C(git checkout XXX);
    C-->D(git add .);
    D-->E("git commit -m '[XXX] XXX'");
    E-->F("git checkout master ");
    F-->G(git pull);
    G-->H["..."]
```

```mermaid
graph LR;
    G["..."]-->H(git checkout XXX);
    H-->I("git merge master ");
    I-->Itest{test localhost}; 
    Itest-->J(git checkout master);
    J-->K(git pull);
    K-->L(git merge XXX);
    L-->M["..."];
```

```mermaid
graph LR;
    C["..."] --> D(git push)
    D --> F((nouvelle fonctionnalité))
    F-->G("Nouvelle branche ? Repartir de git branch XXX ");
    F-->H("Reprendre une branche existante ? Repartir de git checkout XXX");
```

###############################################



```mermaid
graph LR;
    A(git clone)-->B(git Branch XXX);
    B-->C(git checkout XXX);
    C-->D(git add .);
    D-->E("git commit -m '[XXX] XXX'");
    E-->F("git checkout (master - dev ?)");
    F-->G(git pull);
    G-->H["..."]
```

```mermaid
graph LR;
    G["..."]-->H(git checkout XXX);
    H-->I("git merge (master - dev ?)");
    I-->Itest{test localhost:3000}; 
    Itest-->J(git checkout dev);
    J-->K(git pull);
    K-->L(git merge XXX);
    L-->M["..."];
```
```mermaid
graph LR;
    AA["..."]-->A(git push);
    A-->Atest{test Heroku-dev};
    Atest-->B(git checkout master);
    B--> C(git pull);
    C--> D(git merge dev);
    D--> E["..."];
```

```mermaid
graph LR;
    C["..."] --> D(git push)
    D(git push) --> E{Vérification sur Heroku master};
    E --> F((nouvelle fonctionnalité))
    F-->G("Nouvelle branche ? Repartir de git branch XXX ");
    F-->H("Reprendre une branche existante ? Repartir de git checkout XXX");
```




## la team

- Anis Bouhouche
- Florian Chazot
- Clément Coquille
- Guillaume Silvent
- Roseline Soukamkian


